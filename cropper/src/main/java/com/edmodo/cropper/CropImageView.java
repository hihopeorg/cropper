/*
 * Copyright 2013, Edmodo, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this work except in compliance with the License.
 * You may obtain a copy of the License in the LICENSE file, or at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

package com.edmodo.cropper;

import com.edmodo.cropper.cropwindow.edge.Edge;
import com.edmodo.cropper.cropwindow.handle.Handle;
import com.edmodo.cropper.util.AspectRatioUtil;
import com.edmodo.cropper.util.HandleUtil;
import com.edmodo.cropper.util.PaintUtil;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;
import ohos.media.image.PixelMap;
import ohos.media.image.common.ImageInfo;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import java.io.IOException;

/**
 * Custom view that provides cropping capabilities to an image.
 */
public class CropImageView extends Image implements Component.DrawTask, Component.TouchEventListener {

    // Private Constants ///////////////////////////////////////////////////////////////////////////

    @SuppressWarnings("unused")
    private static final String TAG = CropImageView.class.getName();

    @SuppressWarnings("unused")
    public static final int GUIDELINES_OFF = 0;
    public static final int GUIDELINES_ON_TOUCH = 1;
    public static final int GUIDELINES_ON = 2;

    // Member Variables ////////////////////////////////////////////////////////////////////////////

    // The Paint used to draw the white rectangle around the crop area.
    private Paint mBorderPaint;

    // The Paint used to draw the guidelines within the crop area when pressed.
    private Paint mGuidelinePaint;

    // The Paint used to draw the corners of the Border
    private Paint mCornerPaint;

    // The Paint used to darken the surrounding areas outside the crop area.
    private Paint mSurroundingAreaOverlayPaint;

    // The radius (in pixels) of the touchable area around the handle.
    // We are basing this value off of the recommended 48dp touch target size.
    private float mHandleRadius;

    // An edge of the crop window will snap to the corresponding edge of a
    // specified bounding box when the crop window edge is less than or equal to
    // this distance (in pixels) away from the bounding box edge.
    private float mSnapRadius;

    // Thickness of the line (in pixels) used to draw the corner handle.
    private float mCornerThickness;

    // Thickness of the line (in pixels) used to draw the border of the crop window.
    private float mBorderThickness;

    // Length of one side of the corner handle.
    private float mCornerLength;

    // The bounding box around the Bitmap that we are cropping.
    private RectFloat mBitmapRect = new RectFloat();

    // Holds the x and y offset between the exact touch location and the exact
    // handle location that is activated. There may be an offset because we
    // allow for some leeway (specified by 'mHandleRadius') in activating a
    // handle. However, we want to maintain these offset values while the handle
    // is being dragged so that the handle doesn't jump.
    private Point mTouchOffset = new Point();

    // The Handle that is currently pressed; null if no Handle is pressed.
    private Handle mPressedHandle;

    // Flag indicating if the crop area should always be a certain aspect ratio (indicated by mTargetAspectRatio).
    private boolean mFixAspectRatio;

    // Current aspect ratio of the image.
    private int mAspectRatioX = 1;
    private int mAspectRatioY = 1;

    // Mode indicating how/whether to show the guidelines; must be one of GUIDELINES_OFF, GUIDELINES_ON_TOUCH, GUIDELINES_ON.
    private int mGuidelinesMode = 1;
    private boolean isFirst = true;

    // Constructors ////////////////////////////////////////////////////////////////////////////////

    public CropImageView(Context context) {
        super(context);
        init(context, null);
    }

    public CropImageView(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CropImageView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttrSet attrs) {
        if (attrs != null && attrs.getAttr(CropImageViewAttrSet.guidelines).isPresent()) {
            mGuidelinesMode = attrs.getAttr(CropImageViewAttrSet.guidelines).get().getIntegerValue();
        }
        if (attrs != null && attrs.getAttr(CropImageViewAttrSet.fixAspectRatio).isPresent()) {
            mFixAspectRatio = attrs.getAttr(CropImageViewAttrSet.fixAspectRatio).get().getBoolValue();
        }
        if (attrs != null && attrs.getAttr(CropImageViewAttrSet.aspectRatioX).isPresent()) {
            mAspectRatioX = attrs.getAttr(CropImageViewAttrSet.aspectRatioX).get().getIntegerValue();
        }
        if (attrs != null && attrs.getAttr(CropImageViewAttrSet.aspectRatioY).isPresent()) {
            mAspectRatioY = attrs.getAttr(CropImageViewAttrSet.aspectRatioY).get().getIntegerValue();
        }

        final ResourceManager resources = context.getResourceManager();

        try {
            mBorderPaint = PaintUtil.newBorderPaint(resources);
            mGuidelinePaint = PaintUtil.newGuidelinePaint(resources);
            mSurroundingAreaOverlayPaint = PaintUtil.newSurroundingAreaOverlayPaint(resources);
            mCornerPaint = PaintUtil.newCornerPaint(resources);
            mHandleRadius = resources.getElement(ResourceTable.Float_target_radius).getFloat();
            mSnapRadius = resources.getElement(ResourceTable.Float_snap_radius).getFloat();
            mBorderThickness = resources.getElement(ResourceTable.Float_border_thickness).getFloat();
            mCornerThickness = resources.getElement(ResourceTable.Float_corner_thickness).getFloat();
            mCornerLength = resources.getElement(ResourceTable.Float_corner_length).getFloat();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        addDrawTask(this::onDraw);
        setTouchEventListener(this::onTouchEvent);
    }


    // View Methods ////////////////////////////////////////////////////////////////////////////////



    @Override
    public void postLayout() {
        super.postLayout();
        mBitmapRect = getBitmapRect();
        initCropWindow(mBitmapRect);
        invalidate();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if(isFirst){
            final PixelMap pixelMap = getPixelMap();
            final PixelMapElement drawable = new PixelMapElement(pixelMap);
            final int dwidth = pixelMap.getImageInfo().size.width;
            final int dheight = pixelMap.getImageInfo().size.height;
            final int vwidth = getWidth() - getPaddingLeft() - getPaddingRight();
            final int vheight = getHeight() - getPaddingTop() - getPaddingBottom();
            Matrix matrix = new Matrix();
            float scale;
            float dx;
            float dy;
            if (dwidth <= vwidth && dheight <= vheight) {
                scale = 1.0f;
            } else {
                scale = Math.min((float) vwidth / (float) dwidth,
                        (float) vheight / (float) dheight);
            }
            dx = Math.round((vwidth - dwidth * scale) * 0.5f);
            dy = Math.round((vheight - dheight * scale) * 0.5f);
            matrix.setScale(scale, scale);
            matrix.postTranslate(dx, dy);
            final float scaleX = matrix.getScaleX();
            final float scaleY = matrix.getScaleY();
            final int drawableIntrinsicWidth = drawable.getWidth();
            final int drawableIntrinsicHeight = drawable.getHeight();
            final int drawableDisplayWidth = Math.round(drawableIntrinsicWidth * scaleX);
            final int drawableDisplayHeight = Math.round(drawableIntrinsicHeight * scaleY);
            setWidth(drawableDisplayWidth);
            setHeight(drawableDisplayHeight);
            mBitmapRect = getBitmapRect();
            initCropWindow(mBitmapRect);
            isFirst = false;
        }
        drawDarkenedSurroundingArea(canvas);
        drawGuidelines(canvas);
        drawBorder(canvas);
        drawCorners(canvas);
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        // If this View is not enabled, don't allow for touch interactions.
        if (!isEnabled()) {
            return false;
        }

        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                MmiPoint point = touchEvent.getPointerPosition(0);
                onActionDown(point.getX(), point.getY());
                return true;

            case TouchEvent.PRIMARY_POINT_UP:
            case TouchEvent.CANCEL:
                onActionUp();
                return true;

            case TouchEvent.POINT_MOVE:
                MmiPoint point2 = touchEvent.getPointerPosition(0);
                onActionMove(point2.getX(), point2.getY());
                return true;

            default:
                return false;
        }
    }

    // Public Methods //////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the guidelines for the CropOverlayView to be either on, off, or to show when resizing
     * the application.
     *
     * @param guidelinesMode Integer that signals whether the guidelines should be on, off, or only
     *                       showing when resizing.
     */
    public void setGuidelines(int guidelinesMode) {
        mGuidelinesMode = guidelinesMode;
        invalidate(); // Request onDraw() to get called again.
    }

    public int getGuideLines(){
        return mGuidelinesMode;
    }

    /**
     * Sets whether the aspect ratio is fixed or not; true fixes the aspect ratio, while false
     * allows it to be changed.
     *
     * @param fixAspectRatio Boolean that signals whether the aspect ratio should be maintained.
     * @see {@link #setAspectRatio(int, int)}
     */
    public void setFixedAspectRatio(boolean fixAspectRatio) {
        mFixAspectRatio = fixAspectRatio;
        postLayout(); // Request measure/layout to be run again.
    }

    public boolean isFixedAspectRatio(){
        return mFixAspectRatio;
    }

    /**
     * Sets the both the X and Y values of the aspectRatio. These only apply iff fixed aspect ratio
     * is set.
     *
     * @param aspectRatioX new X value of the aspect ratio; must be greater than 0
     * @param aspectRatioY new Y value of the aspect ratio; must be greater than 0
     * @see {@link #setFixedAspectRatio(boolean)}
     */
    public void setAspectRatio(int aspectRatioX, int aspectRatioY) {

        if (aspectRatioX <= 0 || aspectRatioY <= 0) {
            throw new IllegalArgumentException("Cannot set aspect ratio value to a number less than or equal to 0.");
        }
        mAspectRatioX = aspectRatioX;
        mAspectRatioY = aspectRatioY;

        if (mFixAspectRatio) {
            postLayout(); // Request measure/layout to be run again.
        }
    }

    public int getAspectRatioX(){
        return mAspectRatioX;
    }

    public int getAspectRatioY(){
        return mAspectRatioY;
    }

    /**
     * Gets the cropped image based on the current crop window.
     *
     * @return a new Bitmap representing the cropped image
     */
    public PixelMap getCroppedImage() {

        // Implementation reference: http://stackoverflow.com/a/26930938/1068656

        final PixelMap pixelMap = getPixelMap();
        final PixelMapElement drawable = new PixelMapElement(pixelMap);
        if (drawable == null || !(drawable instanceof PixelMapElement)) {
            return null;
        }
        final int dwidth = pixelMap.getImageInfo().size.width;
        final int dheight = pixelMap.getImageInfo().size.height;

        final int vwidth = getWidth() - getPaddingLeft() - getPaddingRight();
        final int vheight = getHeight() - getPaddingTop() - getPaddingBottom();
        Matrix matrix = new Matrix();
        float scale;
        float dx;
        float dy;

        if (dwidth <= vwidth && dheight <= vheight) {
            scale = 1.0f;
        } else {
            scale = Math.min((float) vwidth / (float) dwidth,
                    (float) vheight / (float) dheight);
        }

        dx = Math.round((vwidth - dwidth * scale) * 0.5f);
        dy = Math.round((vheight - dheight * scale) * 0.5f);

        matrix.setScale(scale, scale);
        matrix.postTranslate(dx, dy);

        // Get image matrix values and place them in an array.
        float[] matrixValues = new float[9];
        matrixValues = matrix.getData();

        // Extract the scale and translation values. Note, we currently do not handle any other transformations (e.g. skew).
        final float scaleX = matrixValues[0];
        final float scaleY = matrixValues[4];
        final float transX = matrixValues[2];
        final float transY = matrixValues[5];

        // Ensure that the left and top edges are not outside of the ImageView bounds.
        final float bitmapLeft = (transX < 0) ? Math.abs(transX) : 0;
        final float bitmapTop = (transY < 0) ? Math.abs(transY) : 0;

        // Get the original bitmap object.
        final PixelMap originalBitmap = getPixelMap();

        // Calculate the top-left corner of the crop window relative to the ~original~ bitmap size.
        final float cropX = (bitmapLeft + Edge.LEFT.getCoordinate()) / scaleX;
        final float cropY = (bitmapTop + Edge.TOP.getCoordinate()) / scaleY;

        // Calculate the crop window size relative to the ~original~ bitmap size.
        // Make sure the right and bottom edges are not outside the ImageView bounds (this is just to address rounding discrepancies).
        ImageInfo imageInfo = originalBitmap.getImageInfo();
        final float cropWidth = Math.min(Edge.getWidth() / scaleX, imageInfo.size.width - cropX);
        final float cropHeight = Math.min(Edge.getHeight() / scaleY, imageInfo.size.height - cropY);

        // Crop the subset from the original Bitmap.
        ohos.media.image.common.Rect rect = new ohos.media.image.common.Rect((int)cropX,(int)cropY,(int)cropWidth,(int)cropHeight);
        return PixelMap.create(originalBitmap, rect,null);
    }

    // Private Methods /////////////////////////////////////////////////////////////////////////////

    /**
     * Gets the bounding rectangle of the bitmap within the ImageView.
     */
    private RectFloat getBitmapRect() {

        final PixelMap pixelMap = getPixelMap();
        final PixelMapElement drawable = new PixelMapElement(pixelMap);
        if (drawable == null) {
            return new RectFloat();
        }

        final int dwidth = pixelMap.getImageInfo().size.width;
        final int dheight = pixelMap.getImageInfo().size.height;

        final int vwidth = getWidth() - getPaddingLeft() - getPaddingRight();
        final int vheight = getHeight() - getPaddingTop() - getPaddingBottom();
        Matrix matrix = new Matrix();
        float scale;
        float dx;
        float dy;

        if (dwidth <= vwidth && dheight <= vheight) {
            scale = 1.0f;
        } else {
            scale = Math.min((float) vwidth / (float) dwidth,
                    (float) vheight / (float) dheight);
        }

        dx = Math.round((vwidth - dwidth * scale) * 0.5f);
        dy = Math.round((vheight - dheight * scale) * 0.5f);

        matrix.setScale(scale, scale);
        matrix.postTranslate(dx, dy);
        // Get image matrix values and place them in an array.
//        float[] matrixValues = new float[9];
//        matrixValues = matrix.getData();
        // Extract the scale and translation values. Note, we currently do not handle any other transformations (e.g. skew).
        final float scaleX = matrix.getScaleX();
        final float scaleY = matrix.getScaleY();
        final float transX = matrix.getTranslateX();
        final float transY = matrix.getTranslateY();

        // Get the width and height of the original bitmap.
        final int drawableIntrinsicWidth = drawable.getWidth();
        final int drawableIntrinsicHeight = drawable.getHeight();

        // Calculate the dimensions as seen on screen.
        final int drawableDisplayWidth = Math.round(drawableIntrinsicWidth * scaleX);
        final int drawableDisplayHeight = Math.round(drawableIntrinsicHeight * scaleY);
        // Get the Rect of the displayed image within the ImageView.
        final float left = Math.max(transX, 0);
        final float top = Math.max(transY, 0);
        final float right = Math.min(left + drawableDisplayWidth, getWidth());
        final float bottom = Math.min(top + drawableDisplayHeight, getHeight());
        return new RectFloat(left, top, right, bottom);
    }

    /**
     * Initialize the crop window by setting the proper {@link Edge} values.
     * <p/>
     * If fixed aspect ratio is turned off, the initial crop window will be set to the displayed
     * image with 10% margin. If fixed aspect ratio is turned on, the initial crop window will
     * conform to the aspect ratio with at least one dimension maximized.
     */
    private void initCropWindow(RectFloat bitmapRect) {

        if (mFixAspectRatio) {

            // Initialize the crop window with the proper aspect ratio.
            initCropWindowWithFixedAspectRatio(bitmapRect);

        } else {

            // Initialize crop window to have 10% padding w/ respect to Drawable's bounds.
            final float horizontalPadding = 0.1f * bitmapRect.getWidth();
            final float verticalPadding = 0.1f * bitmapRect.getHeight();

            Edge.LEFT.setCoordinate(bitmapRect.left + horizontalPadding);
            Edge.TOP.setCoordinate(bitmapRect.top + verticalPadding);
            Edge.RIGHT.setCoordinate(bitmapRect.right - horizontalPadding);
            Edge.BOTTOM.setCoordinate(bitmapRect.bottom - verticalPadding);
        }
    }

    private void initCropWindowWithFixedAspectRatio(RectFloat bitmapRect) {

        // If the image aspect ratio is wider than the crop aspect ratio,
        // then the image height is the determining initial length. Else, vice-versa.
        if (AspectRatioUtil.calculateAspectRatio(bitmapRect) > getTargetAspectRatio()) {

            final float cropWidth = AspectRatioUtil.calculateWidth(bitmapRect.getHeight(), getTargetAspectRatio());

            Edge.LEFT.setCoordinate(bitmapRect.getCenter().getPointX() - cropWidth / 2f);
            Edge.TOP.setCoordinate(bitmapRect.top);
            Edge.RIGHT.setCoordinate(bitmapRect.getCenter().getPointX() + cropWidth / 2f);
            Edge.BOTTOM.setCoordinate(bitmapRect.bottom);

        } else {

            final float cropHeight = AspectRatioUtil.calculateHeight(bitmapRect.getWidth(), getTargetAspectRatio());

            Edge.LEFT.setCoordinate(bitmapRect.left);
            Edge.TOP.setCoordinate(bitmapRect.getCenter().getPointY() - cropHeight / 2f);
            Edge.RIGHT.setCoordinate(bitmapRect.right);
            Edge.BOTTOM.setCoordinate(bitmapRect.getCenter().getPointY() + cropHeight / 2f);
        }
    }

    private void drawDarkenedSurroundingArea(Canvas canvas) {

        final RectFloat bitmapRect = mBitmapRect;

        final float left = Edge.LEFT.getCoordinate();
        final float top = Edge.TOP.getCoordinate();
        final float right = Edge.RIGHT.getCoordinate();
        final float bottom = Edge.BOTTOM.getCoordinate();

        /*-
          -------------------------------------
          |                top                |
          -------------------------------------
          |      |                    |       |
          |      |                    |       |
          | left |                    | right |
          |      |                    |       |
          |      |                    |       |
          -------------------------------------
          |              bottom               |
          -------------------------------------
         */

        // Draw "top", "bottom", "left", then "right" quadrants according to diagram above.
        canvas.drawRect(bitmapRect.left, bitmapRect.top, bitmapRect.right, top, mSurroundingAreaOverlayPaint);
        canvas.drawRect(bitmapRect.left, bottom, bitmapRect.right, bitmapRect.bottom, mSurroundingAreaOverlayPaint);
        canvas.drawRect(bitmapRect.left, top, left, bottom, mSurroundingAreaOverlayPaint);
        canvas.drawRect(right, top, bitmapRect.right, bottom, mSurroundingAreaOverlayPaint);
    }

    private void drawGuidelines(Canvas canvas) {

        if (!shouldGuidelinesBeShown()) {
            return;
        }

        final float left = Edge.LEFT.getCoordinate();
        final float top = Edge.TOP.getCoordinate();
        final float right = Edge.RIGHT.getCoordinate();
        final float bottom = Edge.BOTTOM.getCoordinate();

        // Draw vertical guidelines.
        final float oneThirdCropWidth = Edge.getWidth() / 3;

        final float x1 = left + oneThirdCropWidth;
        Point x1StartPoint = new Point(x1,top);
        Point x1StopPoint = new Point(x1,bottom);
        canvas.drawLine(x1StartPoint,x1StopPoint, mGuidelinePaint);
        final float x2 = right - oneThirdCropWidth;
        Point x2StartPoint = new Point(x2,top);
        Point x2StopPoint = new Point(x2,bottom);
        canvas.drawLine(x2StartPoint,x2StopPoint, mGuidelinePaint);

        // Draw horizontal guidelines.
        final float oneThirdCropHeight = Edge.getHeight() / 3;

        final float y1 = top + oneThirdCropHeight;
        Point y1StartPoint = new Point(left,y1);
        Point y1StopPoint = new Point(right,y1);
        canvas.drawLine(y1StartPoint, y1StopPoint, mGuidelinePaint);
        final float y2 = bottom - oneThirdCropHeight;
        Point y2StartPoint = new Point(left,y2);
        Point y2StopPoint = new Point(right,y2);
        canvas.drawLine(y2StartPoint, y2StopPoint, mGuidelinePaint);
    }

    private void drawBorder(Canvas canvas) {

        canvas.drawRect(Edge.LEFT.getCoordinate(),
                Edge.TOP.getCoordinate(),
                Edge.RIGHT.getCoordinate(),
                Edge.BOTTOM.getCoordinate(),
                mBorderPaint);
    }

    private void drawCorners(Canvas canvas) {

        final float left = Edge.LEFT.getCoordinate();
        final float top = Edge.TOP.getCoordinate();
        final float right = Edge.RIGHT.getCoordinate();
        final float bottom = Edge.BOTTOM.getCoordinate();

        // Absolute value of the offset by which to draw the corner line such that its inner edge is flush with the border's inner edge.
        final float lateralOffset = (mCornerThickness - mBorderThickness) / 2f;
        // Absolute value of the offset by which to start the corner line such that the line is drawn all the way to form a corner edge with the adjacent side.
        final float startOffset = mCornerThickness - (mBorderThickness / 2f);
        // Top-left corner: left side
        Point leftTopLater = new Point(left - lateralOffset,top - startOffset);
        Point leftCornerLater = new Point(left - lateralOffset,top + mCornerLength);
        canvas.drawLine(leftTopLater , leftCornerLater , mCornerPaint);
        // Top-left corner: top side
        Point leftTopStart = new Point(left - startOffset,top - lateralOffset);
        Point topCornerStart = new Point(left + mCornerLength,top - lateralOffset);
        canvas.drawLine(leftTopStart ,topCornerStart, mCornerPaint);
        // Top-right corner: right side
        Point rightTopLater = new Point(right + lateralOffset,top - startOffset);
        Point rightCornerLater = new Point(right + lateralOffset,top + mCornerLength);
        canvas.drawLine(rightTopLater ,rightCornerLater, mCornerPaint);
        // Top-right corner: top side
        Point rightTopStart = new Point(right + startOffset,top - lateralOffset);
        Point rightCornerStart = new Point(right - mCornerLength,top - lateralOffset);
        canvas.drawLine(rightTopStart, rightCornerStart, mCornerPaint);
        // Bottom-left corner: left side
        Point leftBottomLater = new Point(left - lateralOffset,bottom + startOffset);
        Point leftBottomCorner = new Point(left - lateralOffset,bottom - mCornerLength);
        canvas.drawLine(leftBottomLater , leftBottomCorner , mCornerPaint);
        // Bottom-left corner: bottom side
        Point leftBottomStart = new Point(left - startOffset,bottom + lateralOffset);
        Point leftBottomCornerStart = new Point(left + mCornerLength,bottom + lateralOffset);
        canvas.drawLine(leftBottomStart , leftBottomCornerStart , mCornerPaint);
        // Bottom-right corner: right side
        Point rightBottomLater = new Point(right + lateralOffset,bottom + startOffset);
        Point rightBottomCorner = new Point(right + lateralOffset,bottom - mCornerLength);
        canvas.drawLine(rightBottomLater , rightBottomCorner , mCornerPaint);
        // Bottom-right corner: bottom side
        Point rightBottomStart = new Point(right + startOffset,bottom + lateralOffset);
        Point rightBottomCornerStart = new Point(right - mCornerLength,bottom + lateralOffset);
        canvas.drawLine(rightBottomStart , rightBottomCornerStart , mCornerPaint);
    }

    private boolean shouldGuidelinesBeShown() {
        return ((mGuidelinesMode == GUIDELINES_ON)
                || ((mGuidelinesMode == GUIDELINES_ON_TOUCH) && (mPressedHandle != null)));
    }

    private float getTargetAspectRatio() {
        return mAspectRatioX / (float) mAspectRatioY;
    }

    /**
     * Handles a {@link TouchEvent#PRIMARY_POINT_DOWN} event.
     *
     * @param x the x-coordinate of the down action
     * @param y the y-coordinate of the down action
     */
    private void onActionDown(float x, float y) {

        final float left = Edge.LEFT.getCoordinate();
        final float top = Edge.TOP.getCoordinate();
        final float right = Edge.RIGHT.getCoordinate();
        final float bottom = Edge.BOTTOM.getCoordinate();

        mPressedHandle = HandleUtil.getPressedHandle(x, y, left, top, right, bottom, mHandleRadius);

        // Calculate the offset of the touch point from the precise location of the handle.
        // Save these values in member variable 'mTouchOffset' so that we can maintain this offset as we drag the handle.
        if (mPressedHandle != null) {
            HandleUtil.getOffset(mPressedHandle, x, y, left, top, right, bottom, mTouchOffset);
            invalidate();
        }
    }

    /**
     * Handles a {@link TouchEvent#PRIMARY_POINT_UP} or {@link TouchEvent#CANCEL} event.
     */
    private void onActionUp() {
        if (mPressedHandle != null) {
            mPressedHandle = null;
            invalidate();
        }
    }

    /**
     * Handles a {@link TouchEvent#POINT_MOVE} event.
     *
     * @param x the x-coordinate of the move event
     * @param y the y-coordinate of the move event
     */
    private void onActionMove(float x, float y) {

        if (mPressedHandle == null) {
            return;
        }

        // Adjust the coordinates for the finger position's offset (i.e. the distance from the initial touch to the precise handle location).
        // We want to maintain the initial touch's distance to the pressed handle so that the crop window size does not "jump".
        x += mTouchOffset.getPointX();
        y += mTouchOffset.getPointY();

        // Calculate the new crop window size/position.
        if (mFixAspectRatio) {
            mPressedHandle.updateCropWindow(x, y, getTargetAspectRatio(), mBitmapRect, mSnapRadius);
        } else {
            mPressedHandle.updateCropWindow(x, y, mBitmapRect, mSnapRadius);
        }
        invalidate();
    }
}
