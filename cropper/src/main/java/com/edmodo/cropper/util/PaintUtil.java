/*
 * Copyright 2013, Edmodo, Inc. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this work except in compliance with the License.
 * You may obtain a copy of the License in the LICENSE file, or at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" 
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language 
 * governing permissions and limitations under the License. 
 */

package com.edmodo.cropper.util;

import com.edmodo.cropper.ResourceTable;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.global.resource.NotExistException;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

/**
 * Utility class for handling all of the Paint used to draw the CropOverlayView.
 */
public class PaintUtil {

    // Public Methods //////////////////////////////////////////////////////////

    /**
     * Creates the Paint object for drawing the crop window border.
     */
    public static Paint newBorderPaint(ResourceManager resourceManager) throws NotExistException, WrongTypeException, IOException {
        final Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE_STYLE);
        paint.setStrokeWidth(resourceManager.getElement(ResourceTable.Float_border_thickness).getFloat());
        paint.setColor(new Color(resourceManager.getElement(ResourceTable.Color_border).getColor()));
        return paint;
    }

    /**
     * Creates the Paint object for drawing the crop window guidelines.
     */
    public static Paint newGuidelinePaint(ResourceManager resourceManager) throws NotExistException, WrongTypeException, IOException {

        final Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE_STYLE);
        paint.setStrokeWidth(resourceManager.getElement(ResourceTable.Float_guideline_thickness).getFloat());
        paint.setColor(new Color(resourceManager.getElement(ResourceTable.Color_guideline).getColor()));

        return paint;
    }

    /**
     * Creates the Paint object for drawing the translucent overlay outside the crop window.
     *
     * @return the new Paint object
     */
    public static Paint newSurroundingAreaOverlayPaint(ResourceManager resourceManager) throws NotExistException, WrongTypeException, IOException {

        final Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL_STYLE);
        paint.setColor(new Color(resourceManager.getElement(ResourceTable.Color_surrounding_area).getColor()));

        return paint;
    }

    /**
     * Creates the Paint object for drawing the corners of the border
     */
    public static Paint newCornerPaint(ResourceManager resourceManager) throws NotExistException, WrongTypeException, IOException {

        final Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE_STYLE);
        paint.setStrokeWidth(resourceManager.getElement(ResourceTable.Float_corner_thickness).getFloat());
        paint.setColor(new Color(resourceManager.getElement(ResourceTable.Color_corner).getColor()));

        return paint;
    }
}
