# Cropper
**本项目是基于开源项目Cropper进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/edmodo/cropper ) 追踪到原项目版本**

#### 项目介绍
 - 项目名称： Cropper
 - 所属系列：ohos的第三方组件适配移植
 - 功能： 裁剪器是一种图像裁剪工具。它提供了一种用XML和编程方式设置图像的方法，并在图像的顶部显示一个可调整大小的裁剪窗口。
 然后调用getCroppedImage()方法将返回由作物窗口标记的位图。
 - 项目移植状态：完成
 - 调用差异：无
 - 项目作者和维护人： hihope
 - 联系方式：hihope@hoperun.com
 - 原项目Doc地址：https://github.com/edmodo/cropper#readme
 - 原项目基线版本：v_2.0.0   sha1:4bceae1c641a922ab73f769abd13365a15dd356e
 - 编程语言：Java
 - 外部库依赖：

#### 效果展示
<img src="screenshot/cropper.gif" />

#### 安装教程
方法1.

1. 下载依赖库har包Cropper.har。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
dependencies {
    implementation 'com.edmodo.ohos:cropper:1.0.1'
}
```


#### 使用说明
#####  基本使用
CopperImageView属性

| Attribute 属性          | Description 描述 |
|:---				     |:---|
| guidelines         | 网格线行数            |
| aspectRatioX         | x比例            |
| aspectRatioY         | y比例            |


### 常见用法：使用CopperImageView

#### XML
```
<com.edmodo.cropper.CropImageView
            ohos:id="$+id:CropImageView"
            ohos:height="match_content"
            ohos:width="match_parent"
            ohos:image_src="$media:butterfly"
            ohos:scale_mode="inside"/>
```


#### 获取裁剪的图片
```
cropImageView.getCroppedImage();
```

#### 版本迭代
 - v1.0.1



#### 版权和许可信息
 - Apache License

