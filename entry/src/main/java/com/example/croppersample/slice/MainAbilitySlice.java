/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.example.croppersample.slice;

import com.edmodo.cropper.CropImageView;
import com.example.croppersample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.media.image.PixelMap;

public class MainAbilitySlice extends AbilitySlice {

    private static final int GUIDELINES_ON_TOUCH = 1;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        final ToggleButton fixedAspectRatioToggleButton = (ToggleButton) findComponentById(ResourceTable.Id_fixedAspectRatioToggle);
        final Text aspectRatioXTextView = (Text) findComponentById(ResourceTable.Id_aspectRatioX);
        final Slider aspectRatioXSeekBar = (Slider) findComponentById(ResourceTable.Id_aspectRatioXSeek);
        final Text aspectRatioYTextView = (Text) findComponentById(ResourceTable.Id_aspectRatioY);
        final Slider aspectRatioYSeekBar = (Slider) findComponentById(ResourceTable.Id_aspectRatioYSeek);
        final RadioContainer rc_guideLines = (RadioContainer) findComponentById(ResourceTable.Id_rc_guidelines);
        final RadioButton rb_off = (RadioButton) findComponentById(ResourceTable.Id_rb_off);
        final CropImageView cropImageView = (CropImageView) findComponentById(ResourceTable.Id_CropImageView);
        final Image croppedImageView = (Image) findComponentById(ResourceTable.Id_croppedImageView);
        final Button cropButton = (Button) findComponentById(ResourceTable.Id_Button_crop);
        fixedAspectRatioToggleButton.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
                cropImageView.setFixedAspectRatio(isChecked);
                cropImageView.setAspectRatio(aspectRatioXSeekBar.getProgress(), aspectRatioYSeekBar.getProgress());
                aspectRatioXSeekBar.setEnabled(isChecked);
                aspectRatioYSeekBar.setEnabled(isChecked);
            }
        });
        aspectRatioXSeekBar.setEnabled(false);
        aspectRatioYSeekBar.setEnabled(false);
        aspectRatioXTextView.setText(String.valueOf(aspectRatioXSeekBar.getProgress()));
        aspectRatioYTextView.setText(String.valueOf(aspectRatioXSeekBar.getProgress()));
        aspectRatioXSeekBar.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean b) {
                if (progress < 1) {
                    aspectRatioXSeekBar.setProgressValue(1);
                }
                cropImageView.setAspectRatio(aspectRatioXSeekBar.getProgress(), aspectRatioYSeekBar.getProgress());
                aspectRatioXTextView.setText(String.valueOf(aspectRatioXSeekBar.getProgress()));
            }

            @Override
            public void onTouchStart(Slider slider) {
                // Do nothing.
            }

            @Override
            public void onTouchEnd(Slider slider) {
                // Do nothing.
            }
        });
        aspectRatioYSeekBar.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean b) {
                if (progress < 1) {
                    aspectRatioYSeekBar.setProgressValue(1);
                }
                cropImageView.setAspectRatio(aspectRatioXSeekBar.getProgress(), aspectRatioYSeekBar.getProgress());
                aspectRatioYTextView.setText(String.valueOf(aspectRatioYSeekBar.getProgress()));
            }

            @Override
            public void onTouchStart(Slider slider) {
                // Do nothing.
            }

            @Override
            public void onTouchEnd(Slider slider) {
                // Do nothing.
            }
        });
        rc_guideLines.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                cropImageView.setGuidelines(i);
            }
        });
        rb_off.setChecked(true);
        cropButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                final PixelMap croppedImage = cropImageView.getCroppedImage();
                croppedImageView.setPixelMap(croppedImage);
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
