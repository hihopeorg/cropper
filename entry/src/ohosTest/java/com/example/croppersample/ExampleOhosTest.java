package com.example.croppersample;

import com.edmodo.cropper.CropImageView;
import com.edmodo.cropper.cropwindow.edge.Edge;
import com.example.croppersample.MainAbility;
import com.example.croppersample.ResourceTable;
import com.utils.EventHelper;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Button;
import ohos.agp.components.RadioButton;
import ohos.agp.components.Slider;
import ohos.agp.components.ToggleButton;
import ohos.agp.utils.RectFloat;
import ohos.media.image.PixelMap;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ExampleOhosTest {

    private MainAbility mainAbility;

    @Before
    public void before(){
        mainAbility = EventHelper.startAbility(MainAbility.class);
        EventHelper.waitForActive(mainAbility,5);
    }

    @After
    public void tearDown(){
        EventHelper.clearAbilities();
    }

    @Test
    public void testTouch(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        CropImageView cropImageView = (CropImageView) mainAbility.findComponentById(ResourceTable.Id_CropImageView);
        float left = Edge.LEFT.getCoordinate();
        float right = Edge.RIGHT.getCoordinate();
        EventHelper.inputSwipe(mainAbility,cropImageView,500,300,700,500,2000);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertNotEquals(left,Edge.LEFT.getCoordinate());
        Assert.assertNotEquals(right,Edge.RIGHT.getCoordinate());
    }

    @Test
    public void testFixedAspectRatio(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        CropImageView cropImageView = (CropImageView) mainAbility.findComponentById(ResourceTable.Id_CropImageView);
        float left = Edge.LEFT.getCoordinate();
        float right = Edge.RIGHT.getCoordinate();
        ToggleButton toggleButton = (ToggleButton) mainAbility.findComponentById(ResourceTable.Id_fixedAspectRatioToggle);
        EventHelper.triggerClickEvent(mainAbility,toggleButton);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertNotEquals(left,Edge.LEFT.getCoordinate());
        Assert.assertNotEquals(right,Edge.RIGHT.getCoordinate());
    }

    @Test
    public void testAspectRatio(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ToggleButton toggleButton = (ToggleButton) mainAbility.findComponentById(ResourceTable.Id_fixedAspectRatioToggle);
        float left = Edge.LEFT.getCoordinate();
        float right = Edge.RIGHT.getCoordinate();
        EventHelper.triggerClickEvent(mainAbility,toggleButton);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Slider aspectRatioX = (Slider) mainAbility.findComponentById(ResourceTable.Id_aspectRatioXSeek);
        Slider aspectRatioY = (Slider) mainAbility.findComponentById(ResourceTable.Id_aspectRatioYSeek);
        CropImageView cropImageView = (CropImageView) mainAbility.findComponentById(ResourceTable.Id_CropImageView);
        EventHelper.inputSwipe(mainAbility,aspectRatioX,100,20,200,20,2000);
        Assert.assertNotEquals(left,Edge.LEFT.getCoordinate());
        Assert.assertNotEquals(right,Edge.RIGHT.getCoordinate());
        left = Edge.LEFT.getCoordinate();
        right = Edge.RIGHT.getCoordinate();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        EventHelper.inputSwipe(mainAbility,aspectRatioY,100,20,200,20,2000);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertNotEquals(left,Edge.LEFT.getCoordinate());
        Assert.assertNotEquals(right,Edge.RIGHT.getCoordinate());
    }

    @Test
    public void testGuide(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        RadioButton rbOn = (RadioButton) mainAbility.findComponentById(ResourceTable.Id_rb_on);
        EventHelper.triggerClickEvent(mainAbility,rbOn);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        CropImageView cropImageView = (CropImageView) mainAbility.findComponentById(ResourceTable.Id_CropImageView);
        Assert.assertEquals("It's not null",cropImageView.getGuideLines(),2);
    }

    @Test
    public void testCrop(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Button crop = (Button) mainAbility.findComponentById(ResourceTable.Id_Button_crop);
        EventHelper.triggerClickEvent(mainAbility,crop);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        CropImageView cropImageView = (CropImageView) mainAbility.findComponentById(ResourceTable.Id_CropImageView);
        Assert.assertNotNull("It's not null",cropImageView.getCroppedImage());
    }



}