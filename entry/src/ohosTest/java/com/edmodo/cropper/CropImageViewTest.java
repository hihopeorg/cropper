package com.edmodo.cropper;

import com.edmodo.cropper.CropImageView;
import com.example.croppersample.ResourceTable;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.element.PixelMapElement;
import ohos.app.Context;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CropImageViewTest {

    @Test
    public void setGuidelines() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        cropImageView.setGuidelines(5);
        assertEquals(cropImageView.getGuideLines(),5);
    }

    @Test
    public void setFixedAspectRatio() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        cropImageView.setPixelMap(com.example.croppersample.ResourceTable.Media_butterfly);
        cropImageView.setFixedAspectRatio(true);
        assertTrue(cropImageView.isFixedAspectRatio());
    }

    @Test
    public void setAspectRatio() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        cropImageView.setPixelMap(com.example.croppersample.ResourceTable.Media_butterfly);
        cropImageView.setAspectRatio(10,10);
        assertEquals(10,cropImageView.getAspectRatioX());
        assertEquals(10,cropImageView.getAspectRatioY());
    }

    @Test
    public void getCroppedImage() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        cropImageView.setPixelMap(ResourceTable.Media_butterfly);
        assertNotNull("It's not null",cropImageView.getCroppedImage());
    }
}